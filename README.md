Simple shooter level game created in Unity, inspired by tutorial series made by Sebastian Lague https://www.youtube.com/watch?v=SviIeTt2_Lc&list=PLFt_AvWsXl0ctd4dgE1F8g3uec4zKNRV0&ab_channel=SebastianLague.<br />
In order to play the game, download the repo and run the Shooter.exe file.<br />

![image](/uploads/d84d2a10eb3bcc66c19a828c4a42be2d/image.png)
